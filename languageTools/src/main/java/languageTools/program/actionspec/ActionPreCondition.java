package languageTools.program.actionspec;

import krTools.language.Query;
import krTools.language.Substitution;
import krTools.parser.SourceInfo;

public class ActionPreCondition {
	/**
	 * A query representing a condition for successful action execution.
	 */
	private final Query precondition;
	/**
	 * The source-info of the pre-condition
	 */
	private final SourceInfo info;

	public ActionPreCondition(Query precondition, SourceInfo info) {
		this.precondition = precondition;
		this.info = info;
	}

	public Query getPreCondition() {
		return this.precondition;
	}

	public SourceInfo getSourceInfo() {
		return this.info;
	}

	public ActionPreCondition applySubst(Substitution subst) {
		return new ActionPreCondition((getPreCondition() == null) ? null : getPreCondition().applySubst(subst),
				getSourceInfo());
	}

	@Override
	public int hashCode() {
		return (this.precondition == null) ? 0 : this.precondition.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || !(obj instanceof ActionPreCondition)) {
			return false;
		}
		ActionPreCondition other = (ActionPreCondition) obj;
		if (this.precondition == null) {
			if (other.precondition != null) {
				return false;
			}
		} else if (!this.precondition.equals(other.precondition)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return this.precondition.toString();
	}
}
