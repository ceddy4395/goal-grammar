package languageTools.codeanalysis;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import cognitiveKr.CognitiveKR;
import cognitiveKrFactory.CognitiveKRFactory;
import cognitiveKrFactory.InstantiationFailedException;
import krTools.language.DatabaseFormula;
import krTools.language.Query;
import krTools.language.Update;
import languageTools.analyzer.mas.Analysis;
import languageTools.program.actionspec.UserSpecAction;
import languageTools.program.agent.Module;
import languageTools.program.mas.UseClause;

/**
 * A formatter that turns an {@link Analysis} into an ontology-style text
 * describing the MAS.
 *
 */
public class MasOntology {

	private static final String BR = "\n";
	private Analysis analysis;
	private CognitiveKR cognitivekr;

	public MasOntology(Analysis analysis) throws InstantiationFailedException {
		this.analysis = analysis;
		this.cognitivekr = CognitiveKRFactory.getCognitiveKR(analysis.getProgram().getKRInterface());
	}

	/**
	 * 
	 * @param file
	 *            the file to get short path of
	 * @return short path with only parent directory and the file's filename.
	 */
	private String shortPath(File file) {
		File parent = file.getParentFile();

		return parent == null ? file.getName() : parent.getName() + File.separator + file.getName();

	}

	@Override
	public String toString() {

		String str = "Analysis of mas: " + shortPath(analysis.getProgram().getSourceFile()) + BR;
		str += "environment: " + shortPath(analysis.getProgram().getEnvironmentfile()) + BR;
		str += "agents: " + analysis.getProgram().getAgentNames() + BR;

		Set<String> modNames = new TreeSet<String>();
		for (Module module : analysis.getModuleDefinitions()) {
			modNames.add(module.getName());
		}
		str += "modules: " + modNames + BR;

		str += "KR files:" + BR;
		Set<UseClause> set = new HashSet<>();
		set.addAll(analysis.getKrBeliefFiles());
		set.addAll(analysis.getKrGoalFiles());
		set.addAll(analysis.getKrKnowledgeFiles());

		for (UseClause use : set) {
			List<String> usedAs = new ArrayList<>();
			if (analysis.getKrBeliefFiles().contains(use)) {
				usedAs.add("belief");
			}
			if (analysis.getKrGoalFiles().contains(use)) {
				usedAs.add("goal");
			}
			if (analysis.getKrKnowledgeFiles().contains(use)) {
				usedAs.add("knowledge");
			}

			str += "  " + use.getResolvedReference().getName() + " used as " + usedAs + BR;
		}

		str += "Predicate info:" + BR;
		// Unused predicates
		Set<String> unused = new HashSet<String>();
		for (DatabaseFormula formula : analysis.getBeliefsUnused()) {
			// CHECK should we include only the top level formula?
			unused.addAll(cognitivekr.getUsedSignatures(formula));
		}
		str += "  Unused predicates:" + unused + BR;

		// percept predicates
		Set<String> perceptQs = new TreeSet<String>();
		for (Query query : analysis.getPerceptQueries()) {
			perceptQs.addAll(cognitivekr.getUsedSignatures(query));
		}
		str += "  All percept predicates:" + setToString(perceptQs, 4) + BR;

		// belief queries.
		Set<String> beliefPreds = new TreeSet<String>();
		for (DatabaseFormula belief : analysis.getAllBeliefs()) {
			beliefPreds.addAll(cognitivekr.getUsedSignatures(belief));
			;
		}
		for (DatabaseFormula belief : analysis.getAllKnowledge()) {
			beliefPreds.addAll(cognitivekr.getUsedSignatures(belief));
		}
		for (Query query : analysis.getPredicateQueries()) {
			beliefPreds.addAll(cognitivekr.getUsedSignatures(query));
		}
		str += "  All belief predicates:" + setToString(beliefPreds, 4) + BR;

		// goal predicates
		Set<String> goalPreds = new TreeSet<String>();
		for (Query query : analysis.getGoalQueries()) {
			goalPreds.addAll(cognitivekr.getUsedSignatures(query));
		}
		for (DatabaseFormula dbf : analysis.getGoalDBFs()) {
			goalPreds.addAll(cognitivekr.getUsedSignatures(dbf));
		}
		str += "  All goal predicates:" + setToString(goalPreds, 4) + BR;

		// insert updates
		Set<String> insertPreds = new TreeSet<String>();
		for (Update update : analysis.getInsertUpdates()) {
			insertPreds.addAll(cognitivekr.getUsedSignatures(update));
		}
		str += "  All insert predicates: " + setToString(insertPreds, 4) + BR;

		// delete updates
		Set<String> deletePreds = new TreeSet<String>();
		for (Update update : analysis.getDeleteUpdates()) {
			deletePreds.addAll(cognitivekr.getUsedSignatures(update));
		}
		str += "  All delete predicates: " + setToString(deletePreds, 4) + BR;

		// adopt updates
		Set<String> adoptPreds = new TreeSet<String>();
		for (Update update : analysis.getAdoptUpdates()) {
			adoptPreds.addAll(cognitivekr.getUsedSignatures(update));
		}
		str += "  All adopt predicates: " + setToString(adoptPreds, 4) + BR;

		// drop updates
		Set<String> dropPreds = new TreeSet<String>();
		for (Update update : analysis.getDropUpdates()) {
			dropPreds.addAll(cognitivekr.getUsedSignatures(update));
		}
		str += "  All drop predicates: " + setToString(dropPreds, 4) + BR;

		str += "Action info:" + BR;
		// notice, an action can be both used and unused. Unfortunately we can't
		// find the used anymore.
		Set<String> usedActions = new TreeSet<String>();
		for (UserSpecAction action : analysis.getActionDefinitions()) {
			usedActions.add(action.getSignature());
		}
		str += "  Used actions:" + setToString(usedActions, 4) + BR;

		Set<String> unusedActions = new TreeSet<String>();
		for (UserSpecAction action : analysis.getUnusedActionDefinitions()) {
			unusedActions.add(action.getSignature());
		}
		str += "  Unused actions:" + setToString(unusedActions, 4) + BR;

		str += "Rule counts" + BR;
		str += "  event modules: " + ruleCount(analysis.getEventModules()) + BR;
		str += "  init modules: " + ruleCount(analysis.getInitModules()) + BR;

		Set<Module> remaining = new HashSet<>(analysis.getModuleDefinitions());
		remaining.removeAll(analysis.getEventModules());
		remaining.removeAll(analysis.getInitModules());
		str += "  other modules:" + BR;
		for (Module module : remaining) {
			str += "    " + module.getSignature() + ":" + module.getRules().size() + BR;
		}

		return str;
	}

	/**
	 * @param modules
	 *            the modules to count
	 * @return total #rules in the set of modules.
	 */
	private int ruleCount(Set<Module> modules) {
		int ruleCount = 0;
		for (Module mod : modules) {
			ruleCount += mod.getRules().size();
		}
		return ruleCount;

	}

	/**
	 * @param list
	 *            elements to put in the string
	 * @param indentation
	 *            the indentation for each element
	 * @return string with each element of the list on a new line, prepended
	 *         with #indentation whitespaces
	 */
	private String setToString(Set<String> list, int indentation) {
		String ind$ = "\n", res = "";

		for (int n = 0; n < indentation; n++)
			ind$ += " ";

		for (String element : list) {
			res += ind$ + element;
		}
		return res;
	}
}
